# 60-Extensions-to-Supercharge-Visual-Studio-Code
Visual Studio Code is absolutely awesome. Even out of the box, the editor can already do so much, especially for web developers. One of the major reasons that it’s so popular is that it has this unique ability to go from a strict text editor to a near full-blown IDE.  This is thanks to the fact that VSCode is extremely customizable. Every action you can think of in the editor? If it doesn’t already have a shortcut, you can definitely set one! Don’t like the color scheme? Yeah, there’s a setting for that too. But the real power of VSCode comes from its amazing extension marketplace.  Here’s a list of most of the extensions I make use of while using VSCode. I categorize them into four separate groups: general, specific, debugger, and snippet extensions. I definitely do not use all of these on a daily basis, but I’m also pretty good at getting rid of the ones I don’t need, or don’t make use of.

----
## General Extensions

> These extensions are useful no matter what kind of language you’re writing code in!

* Settings Sync

If you work from multiple computers (like from work and home), this is a definite must-have. It doesn’t sync just your settings, it syncs all of your other extensions as well! It works by saving your complete VSCode setup to a GitHub Gist, and when you want to add another PC to sync with, all you have to do is put in your GitHub token and Gist ID!

* Intellicode

If you take advantage of VSCode Intellisense, this extension makes that feature even better. The extension has AI-scanned a few languages and looks at the most common actions performed (for example, array.length). When VSCode actives Intellisense, it puts these common actions at the top of the list, denoting them with a star.

* Live Share

Collaboration is key! Live share lets you share a workspace (that includes both editors and terminals, too) with other people. It connects with your GitHub account, making working on a small group project, or doing pair coding incredibly easy. If you want to quickly sync everyone up during a hackathon, this is definitely the way to go.

* Code Time

Want to track your productivity? Code Time keeps stats on all kinds of things, from how long your coding sessions are, how many lines and words per day you average, even what your most productive time of day is. All of this presented either directly in VSCode, or from a neatly designed web dashboard. You can make a free account with them too, to keep track between multiple computers as well!

* Git History

If you want to find out who wrote the horrible line of code you’ve discovered in your codebase, Git History’s got you covered. It lets you see a color-coded history of any branch in the repository you’re currently in. It also lets you compare branches, search for specific changes and commits, and visually merge or rebase.

* TODO List

Ever wonder if you’ve gotten to all of those TODO’s and FIXME’s before shipping that new feature to production? This extension’s got you covered, it adds a drawer to the Explorer view, listing out all of those to-do’s lying around your codebase. Oh, and you can export all of them to a Trello board or an email as well, did I mention that?

* Polacode

Ever want to take those slick looking screenshots of your code for twitter? That’s what this little extension does, and does very well. It uses the color scheme and font that you’re using to capture a stunning, high-resolution screenshot of whatever you currently have highlighted.

* Color Info

This one’s more for the designers, it adds a context menu popup that gives more info on selected color. Get the RGBA, CMYK, Hexadecimal, or HSL color info, or change the color directly using a nice color picker.

* Emoji Sense

Emojis. Put them everywhere, hide them in your internal documentation for your coworkers to find, stick them in your README’s. Fairly straightforward, it lets you put emojis in whatever file you’re editing.

* SmoothType

SmoothType of those tiny tweaks that's barely noticeable, but is definitely one of those small quality-of-life changes that make your editing experience better. All it does is make the movements of your cursor animated, instead of that instant jump when your cursor moves to a different part of the page.

* Guides

VSCode actually has built-in guidelines (make sure you disable those after you install this one), but this makes those guidelines better. It’ll show you what indentation level you’re currently working at, hide guidelines when making a selection, and has better syntax support for various languages.

* Output Colorizer

Another one of those small quality-of-life extensions that doesn’t add any actual functionality, instead it adds some color to the output panel, as well as syntax coloring to log files. Just another small change, but definitely one that makes the editor just that much more enjoyable.

* Vim

There’s a reason that I saved this one for the last of the general extensions, it’s a heavy hitter. This extension takes blazingly fast editing speed of the Vim editor and puts it right into VSCode. It’s great if you want to dip your toe into a bit of Vim, but personally, I love it because I get to get the speed of Vim combined with the power of VSCode. Not to mention that it still uses most of your .vimrc configuration, doubling the community of editor extensibility and plugins you can draw from.

## Specific Extensions

These are fairly language or file specific extensions. Personally, I write a lot of Javascript and Python, and so I have a lot of web and scripting related extensions!

* Auto Close Tag

Insert an HTML tag, and this extension will automatically add the closing tag. This one alone can cut the time you have to spend writing HTML markup in half.

* Auto Rename Tag

Another HTML extension, except this one renames the matching tag if you change the other tag. This works no matter if you change the opening or closing tag as well.

* Better Jinja

Jinja is a templating language used by the Flask framework. Better Jinja takes your default syntax coloring scheme and applies it to every Jinja file format, including .jinja, .jinja2, and .j2.

* CDNjs

Finding all of the CDNs for the quick libraries you want to pull in for that proof-of-concept you’re working on can be annoying. CDNjs takes care of all of that, adding in a search feature to find the right CDN you need. Especially useful for pulling in stuff like Bootstrap and Font Awesome.

* Docker

Make connecting to your containers easier than ever. Docker lets you add and manage all of your containers, images, and registries in a centralized location. (Especially useful for those of you who want to use your setup for work!)

* DotENV

Really straightforward extension, adds syntax highlighting to your .env files.

* ESLint

Using a linter can be the difference between good and great code. ESLint connects directly to your ESLint install, and integrations problem finding right into your editor. Write clean code, and catch those issues before you even know they’re issues.

* Excel Viewer

If you do any heavy lifting with data, especially with excel or .csv files, Excel Viewer is for you. It adds a nicely formatted viewing table for those types of files directly into VSCode.

* GI

Sometimes you forget to add that .gitignore file when you’re creating your repository. It happens to the best of us, and you sometimes don’t realize it until you’ve accidentally committed your node_modules folder to your repo. Gi lets you add that pesky .gitignore file directly from VSCode, and has presents for just about everything.

* GraphQL

Syntax highlighting and more for GraphQL. Works in both .gql and .graphql files, as well as in ES6 gql template literal calls (for those of you who use Apollo). It also adds some really nice-to-haves, like definition jumping, schema validation, and snippets.

* Handlebars

Slightly obsolete now thanks to React, I do still find myself working in Handlebars from time to time. Handlebars adds two super simple things: syntax coloring, and some useful snippets.

* HTML Class Suggestions

If you’ve ever used a CSS framework, trying to remember even half a dozen of the many classes provided to you can be a nightmare. You end up having to keep a tab open to the documentation and have to constantly search around looking for a class you know the vague name of. No more, thanks to this one! This extension will put CSS class suggestions for you to use based on the linked stylesheets.

* HTML Tag Wrap

If I ever need to quickly surround a tag with another tag, surround.vim usually has me covered. This extension takes that idea to the next level, adding support for Emmet expansion. Make sure to set up a keybind for this one (I have mine set to Opt + Shit + E), and then proceed to edit HTML at the speed of thought.

* Image Minify

Quickly compress your pictures with this image minifier. You don’t lose quality and are able to deliver your content to the end-user faster. It’s a win-win situation.

* Lit It

Despite the cliché name, this extension helps you stub out jsdoc function docstrings. The generated stubs are usually pretty accurate too, as the extension looks at the method signature before generating the stub. All you have to do is write the important stuff.

* Live Server

If you’re a front-end developer of any sort, this extension is an absolute must-have. It spins up a dummy static server so you can test how content might actually look when it gets requested by the browser. Especially useful for when you run into those odd bugs just because you’re using the file:// protocol in Chrome.

* Markdown PDF

Write it in Markdown, export a PDF as output. Straight to the point, but surprisingly useful.

31. Markdown Preview Enhanced

Like most sane people, I have a dark color scheme for VSCode. Unfortunately, the built-in Markdown viewer for VSCode will take whatever your default background is for your editor, and use it as your Markdown background viewer. If you want to see what your README will actually look like directly in your editor, this one’s got you covered.

* Mongo Runner

MongoDB is one of two databases that I’m almost always working with. Being able to run queries and do some basic database management from within VSCode without having to open Compass is such a time saver.

* MySQL

MySQL is the other database I’m usually working with. This extension isn’t as powerful as it’s Mongo counterpart, but it’s my preferred MySQL extension. It lets you add and manage multiple database connections, and run queries, which is usually all I need it for.

* Node TDD

Test-driven development has a nice output of producing very nice code that you already know is working, as you already have tests to tell you so. This extension lets you look to quickly see if your tests are passing at a glance.

* NPM

Run NPM scripts directly from the command palette. The really interesting thing about this extension is that it connects to the touch bar on the new MacBooks Pros. It will also help you out by doing some preliminary package.json validation for you as well.

* NPM Intellisense

Give Intellisense an upgrade and let it suggest what packages to import. This is especially useful for when you need to be efficient with what exactly you’re importing, in order to keep those bundle sizes down.

* Open in Browser

The majority of the time, you can just use Live Server to do the same thing. The reason I still keep this one installed is that it can also detect other browsers you have installed on your local machine, letting you quickly run and test your code quickly across various browsers.

* Paste JSON as Code

If you don’t write your backend with Node.js, defining the structure of a JSON response can be aggravating. This extension helps you out by letting you paste in a sample response (I like to use Postman for that), then generating the structure of the response that you need in your code.

* PDF

I didn’t even know I wanted to be able to view a PDF in VSCode until I found myself splitting my screen to write the code along with the book I was reading. The more I can stay in my editor, the happier I am!

* Prettier

This one can be a little tricky to get set up with ESLint, and will definitely require a bit of configuration to get the two working together smoothly. Once you do have the synced up, they make writing nice looking code a breeze!

* Random

Make writing random snapshot testing data a breeze with this one. This extension can generate random data for just about anything you can think of, names, cities, addresses, phone numbers, you name it.

* Rewrap

Another one of those quality-of-life extensions, it’ll automatically wrap long lines of comments or documentation onto the next line. You can set exactly the amount of characters you want it to wrap too, to keep your formatting on point!

* Sass

Autocompletion and syntax highlighting for Sass files. Also comes with a couple of useful snippets, letting you write your sass code just a bit easier.

* Styled-Components

Along the same school of thought as the GraphQL extension, this one instead adds CSS syntax highlighting for any styled-components template literals. I use a vibrant red color for strings that looks great until you get a giant wall of text, so this is a really nice extension to have. Just makes your component code look complete at a glance.

* SVG Viewer

VSCode has a built-in image viewer which is fantastic, but sometimes you need to view more than just JPEGs and PNGs.

* TSLint

Linting integration, but this one is for TypeScript instead of JavaScript. VSCode does the best it can with JavaScript IntelliSense, but with TypeScript, strong types just make it that much better.

* Vetur

Autocompletion, syntax coloring, Emmet support, and more for all of your markup, CSS, and JavaScript inside your .vue files. Without this extension, writing single-file-components in Vue can get quite messy.

* Vue Peek

Peeking is probably the most underrated feature of VSCode. This extension is for when you quickly need to look at references or definitions of something else inside your current file.

## Debugger Extensions

These extensions are very specific to what language you’re working in, I would definitely look to see if the language you code in has a debugger (spoiler alert: it probably does). Here are all the ones I personally use and recommend:

* Debugger for Chrome
* Python
* React Native Tools

## Snippet Extensions

These are also very specific to what environment you’re working in but are really useful. Snippets take care of all that structural and repetitive code that you end up writing for whatever you’re working on. The more snippets you know, the fast you can write the fun and important parts of your app!

* Django snippets
* ES7/React/Redux/GraphQL snippets
* Javascript/ES6+ snippets
* jQuery snippets
* MongoDB snippets for Node.js
* Python snippets
* React Apollo snippets
* Vue 2 snippets
* Webpack snippets
